# DirectASM

simple language developed for fun :3

## Usage
Run:
```bash
python3 dasm/run.py examples/calc.py
```
Translate to python:
```bash
python3 dasm/dasm2py.py examples/calc.py
