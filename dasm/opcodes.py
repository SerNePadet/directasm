# -*- coding: utf-8 -*-
#     DASM Interpreter (DirectASM Interpreter)
#     Copyright (C) 2020 Cytedge

#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.

#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.

# pylint: disable=eval-used,unused-argument

"""
Opcodes for DirectASM
"""


VERSION = 1.0

op_codes = {
    "": 0,
    "stdout": 1,
    "mov": 2,
    "binary_add": 3,
    "binary_sub": 4,
    "binary_mult": 5,
    "binary_divide": 6,
    "stdin": 8,
    "jmp_dot": 9,
    "_": 10,
    "jmp": 11,
    "cmp": 12,
    "else": 13,
    "convert": 14,
    "global": 15,
    "set": 16,
    "at": 17,
    "call": 18,
    "stdout_disasm": 98,
    "stdout_pycode": 99,
    "exit": 100
}


def translate(text_code, data):
    """Translate func"""
    opcode = op_codes[text_code.lower()]
    part_code = eval(f"op{opcode}(data)")
    return part_code


def op0(args):
    """NULL INSTRUCTION"""
    return ""


def op1(args):
    """STDOUT INSTRUCTION"""
    return """print({}, end="")""".format(args)


def op2(args):
    """MOV INSTRUCTION"""
    spl_args = args.split(", ")
    return """{} = {}""".format(spl_args[0].strip(), spl_args[1].strip())


def op3(args):
    """BINARY_ADD INSTRUCTION"""
    spl_args = args.split(", ")
    return """{} = {}+{}""".format(spl_args[0].strip(), spl_args[1].strip(), spl_args[2].strip())


def op4(args):  # BINARY_SUB
    """BINARY_SUB INSTRUCTION"""
    spl_args = args.split(", ")
    return """{} = {}-{}""".format(spl_args[0].strip(), spl_args[1].strip(), spl_args[2].strip())


def op5(args):  # BINARY_MULT
    """BINARY_MULT INSTRUCTION"""
    spl_args = args.split(", ")
    return """{} = {}*{}""".format(spl_args[0].strip(), spl_args[1].strip(), spl_args[2].strip())


def op6(args):  # BINARY_DIVIDE
    """NULL INSTRUCTION"""
    spl_args = args.split(", ")
    return """{} = {}/{}""".format(spl_args[0].strip(), spl_args[1].strip(), spl_args[2].strip())


def op8(args):  # STDIN
    """STDIN INSTRUCTION"""
    return """{} = input()""".format(args)


def op9(args):  # JMP_DOT
    """JMP_DOT INSTRUCTION"""
    return """def {}():""".format(args)


def op10(args):  # _
    """_ INSTRUCTION"""
    delisted = args.split(" ", 1)
    code = translate(delisted[0], delisted[1]) if len(delisted) > 1 else translate(delisted[0], None)
    return """  {}""".format(code)


def op11(args):  # JMP
    """JMP INSTRUCTION"""
    return """{}()""".format(args)


def op12(args):
    """CMP INSTRUCTION"""
    spl_args = args.split(", ")
    return """if {} == {}:""".format(spl_args[0].strip(), spl_args[1].strip())


def op13(args):  # ELSE
    """ELSE INSTRUCTION"""
    return """else:"""


def op14(args):  # CONVERT
    """CONVERT INSTRUCTION"""
    spl_args = args.split(", ")
    return """{} = {}({})""".format(spl_args[0].strip(), spl_args[1].strip(), spl_args[0])


def op15(args):  # STDIN
    """GLOBAL INSTRUCTION"""
    return """global {}""".format(args)


def op16(args):  # SET
    """SET INSTRUCTION"""
    spl_args = args.split(", ")
    return "{}[{}] = {}".format(spl_args[0], spl_args[1], spl_args[2])


def op17(args):  # AT
    """AT INSTRUCTION"""
    spl_args = args.split(", ")
    return "{} = {}[{}]".format(spl_args[2].strip(), spl_args[0].strip(), spl_args[1])


def op18(args):  # CALL
    """CALL INSTRUCTION"""
    spl_args = args.split(", ")
    return "{}({})".format(spl_args[0], spl_args[1])


def op98(args):
    """STDOUT_DISASM INSTRUCTION"""
    return """# discode"""


def op99(args):
    """STDOUT_PYCODE INSTRUCTION"""
    return """# pycode"""


def op100(args):
    """EXIT INSTRUCTION"""
    return """exit({})""".format(args)
